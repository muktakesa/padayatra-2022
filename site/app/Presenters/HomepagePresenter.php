<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Forms\ContactFormFactory;
use Nette\Utils\DateTime;

final class HomepagePresenter extends BasePresenter {
	/** @var ContactFormFactory @inject */
    public $factory;

    public function renderDefault() {
        $date_now = new DateTime();
        $date_now->setTime(0, 0, 0);
        
        $this->template->references = $this->reference->findBy(['active' => 1])
                                                      ->order('position');
    }

	protected function createComponentContactForm() {
        $form = $this->factory->create();

        $form->onSuccess[] = function ($form, $values) {
            $this->payload->success = true;
            $this->sendPayload();
            $this->terminate();
        };      

        return $form;
    } 
}
