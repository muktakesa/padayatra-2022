<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Utils\DateTime;

final class TourPresenter extends BasePresenter {
	
	public function renderRoute() {
        $this->template->tour_places = $this->tour->findBy(['active' => 1])
        										  ->order('place_date ASC');

		$today = new DateTime;
		$today->setTime(0,0,0);
		$this->template->today = $today;
    }
}
