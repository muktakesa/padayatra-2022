<?php
class Filters {
 
    public static function initialize($filter, $value) {
        if (method_exists(__CLASS__, $filter)) {
            $args = func_get_args();
            array_shift($args);
            return call_user_func_array([__CLASS__, $filter], $args);
        }
    }
        
    public static function dynamicDate($year, $month = "", $day = "") {
        if ($month && $day) { return $day.'.'.$month.'.'.$year; }
        elseif ($month) {     return $month.".".$year; }
        else {                return $year; }
    }   
    
    public static function czDate($date, $format) {
        $en = array("January","February","March","April","May","June","July","August","September","October","November","December");
        $cz = array("led", "úno", "bře", "dub", "kvě", "čvn", "čvc", "srp", "zář", "říj", "lis", "pro");

        $timestamp = strtotime($date);
        $readable_date = date($format, $timestamp);
        $readable_date = str_replace($en, $cz, $readable_date); 
        return $readable_date;
    }

    public static function czInflection($count, $nominative, $genitive, $plural_genitive) {
        if($count == 1)
            return $nominative;
        elseif($count >= 2 && $count <= 4)
            return $genitive;
        else
            return $plural_genitive;
    }
    
    public static function shortify($s, $len = 10) {
        return mb_substr($s, 0, $len);
    }

    public static function dayOfWeekFromDate($date, $locale = "cs") {
        $days_cs = ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"];
        $days_en = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $day_of_week = date("w", strtotime($date));
        $days_array = "days_".$locale;
        return $$days_array[$day_of_week];
    }

    public static function dayOfWeekFromDayNum($dayNum) {
        $days_cs = ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"];
        $days_en = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        $days_array = "days_".$locale;
        return $$days_array[$dayNum];
    }

    public static function weekDayDate($day_num, $year, $week) {
        $date = new DateTime();
        $date->setISODate($year, $week, $day_num);
        return $date;
    }

    public static function phoneReadable($phone_number) {
        $str_rev = strrev(str_replace(' ', '', $phone_number));
        return strrev(substr($str_rev, 0, 3)." ".substr($str_rev, 3, 3)." ".substr($str_rev, 6, 3)." ".substr($str_rev, 9));
    }

    public static function dayOfWeek($date) {
        $days = ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"];

        $day_of_week = date("w", strtotime($date));
        return $days[$day_of_week];
    }  
}