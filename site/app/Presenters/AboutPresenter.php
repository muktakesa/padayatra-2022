<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;


final class AboutPresenter extends BasePresenter {

	public function renderOurOxen() {
		$this->template->oxen = $this->supportOxModel->findAll()
													 ->order('position');
	}
}
