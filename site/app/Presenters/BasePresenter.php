<?php

namespace App\Presenters;

use Nette;
use Tracy\Debugger;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {
    /** @var Tour */
    protected $tour;
    /** @var Constants */
    protected $constantsModel;
    /** @var SupportOx */
    protected $supportOxModel;
    /** @var Reference */
    protected $reference;
    /** @var Page */
    protected $page;

    public function __construct(\App\Model\Tour $tour,
                                \App\Model\Constants $constants,
                                \App\Model\SupportOx $supportOx,
                                \App\Model\Reference $reference,
                                \App\Model\Page $page) {

        parent::__construct();

        $this->tour = $tour;
        $this->constantsModel = $constants;
        $this->supportOxModel = $supportOx;
        $this->reference = $reference;
        $this->page = $page;
    }

    protected function beforeRender() {
        $this->template->addFilter(null, 'Filters::initialize');
        $this->template->constants = $this->constantsModel->findAll()->fetch();
    }        
}
