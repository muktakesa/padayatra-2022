<?php

namespace App\Model;
use Nette;
use Tracy\Debugger;
use Nette\Database\SqlLiteral;
use Nette\Utils\Image;

abstract class TableExtended extends Table  { 

    public function insert($data)	{
        try {
            return parent::insert($data);

        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            $exception = new DuplicateException($e->getMessage());
            $exception->foreign_key = substr($e->getMessage(), strpos($e->getMessage(), "for key '") + 9, -1);
            throw $exception;
		}
	}
 	   
    public function update($id, $data) {
        // kontrola, zda se do některého cizího klíče nepřiřazuje prázdný řetězec, pokud ano nastaví se hodnota klíče na NULL
        $references = $this->connection->getStructure()
                                       ->getBelongsToReference($this->tableName);
        
        foreach($references as $column => $table) {
           if(isset($data[$column]) && $data[$column] == "") {
               $data[$column] = NULL;
           }
        }

        try {
            return parent::update($id, $data);

        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            $exception = new DuplicateException($e->getMessage());
            $exception->foreign_key = substr($e->getMessage(), strpos($e->getMessage(), "for key '") + 9, -1);
            throw $exception;
        }
    }

    public function delete($id) {
        try {
            return parent::delete($id);

        } catch (Nette\Database\ForeignKeyConstraintViolationException $e) {
            return false;
        }
        
    }    

    public function getTitleById($id)  {
        $record = $this->get($id);
        if($record)
            return $record->url;
    }
    
    public function getIdByTitle($url)  {
        $record = $this->findBy(array("url" => $url))->fetch();
        if($record) {
            return $record->id; 
        }
    }    

    public function updatePosition($record_id, $new_position) {
        $record = $this->get($record_id);
        $old_position = $record->position;
                             
        if($old_position != $new_position) {
            $max_position = $this->findAll()
                                 ->max('position');
            
            $record->update(['position' => $new_position]);
                        
            $sign = $old_position < $new_position ? "-" : "+";
        
            $this->findAll()
                 ->where("id != ? AND position BETWEEN ? AND ?", $record_id, min($old_position, $new_position), max($old_position, $new_position))
                 ->update(["position" => new SqlLiteral("position {$sign} 1")]);
        }
        
        return $new_position;
    }

    public function saveImage($image_source, $filename_target, $width = null, $height = null, $flags = Image::FIT, $quality = null, $type = null) {     
        if($width != null || $height != null) {
            $image_source->resize($width, $height, $flags);
        }
        
        $image_source->save($filename_target, $quality, $type);
        chmod($filename_target, 0777);
    }
}

class DuplicateException extends \Exception {
    public $foreign_key;
}