<?php

namespace App\Model;
use Nette;
use Tracy\Debugger;
use Nette\Utils\FileSystem;
use Nette\Database\SqlLiteral;

class Reference extends TableExtended {
    /** @var string */
    protected $tableName = 'reference';

    public function delete($id) {
        $record = $this->get($id);
        
        $this->findAll()
             ->where('position > ?', $record->position)
             ->update(['position' => new SqlLiteral("position - 1")]);
                    
        return $record->delete();           
    }   
}