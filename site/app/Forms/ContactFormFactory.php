<?php

namespace App\Forms;

use Nette;
use Latte;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\Random;
use Tracy\Debugger;

class ContactFormFactory {
	use Nette\SmartObject;

	/** @var FormFactory */
	private $formFactory;
	/** @var Nette\Application\UI\ITemplateFactory */
	private $templateFactory;   

	public function __construct(FormFactory $formFactory, \Nette\Application\UI\ITemplateFactory $templateFactory) {
		$this->formFactory = $formFactory;
		$this->templateFactory = $templateFactory;
	}

	public function create() {
		$form = $this->formFactory->create();
		$form->addProtection();
		$data = $form->addContainer('data');
		
		$hash = Random::generate(10);

		$data->addText('name', 'Jméno')
			 ->setRequired('Prosím vyplňte jméno.');

		$data->addEmail('email', 'Email')
			 ->setRequired('Prosím zadejte email');

        $form->addText('workEmail', 'Work email:')
             ->addRule(Form::BLANK, "Work email");

        $form->addText('workEmailConfirm', 'Work email confirm:');

        $form->addText('workPlace', 'Work email confirm:')
          	 ->setValue($hash);

		$data->addTextArea('text', 'Text')
			 ->setRequired('Prosím napište něco');
		
		$form->addSubmit('send', 'Odeslat');

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}

	public function formSucceeded(Form $form, $values) {
		$values = $form->getValues();

		if($values->workPlace !== $values->workEmailConfirm) { exit; }

		$template = $this->templateFactory->createTemplate();
    	$template->setFile(__DIR__.'/../Presenters/templates/emails/contact-message-email.latte');
    	$template->addFilter(null, 'Filters::initialize');

		$template->data = $values->data;
		
		//$template->render();exit;

    	$mail = new Message;
    	$mail->setFrom($values->data->email)
      		 ->addTo(CONTACT_EMAIL)
         	 ->addBcc('muktakesa.bvks@gmail.com')
        	 ->setSubject("Zpráva z webu padayatra.cz")
		   	 ->setHtmlBody($template);
    
    	$mailer = new SendmailMailer;
    	$send_mail = $mailer->send($mail);
	}
}
