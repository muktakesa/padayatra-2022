<?php

namespace App\AdminModule\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;
use Tracy\Debugger;

class SupportOxFormFactory {
	use Nette\SmartObject;
	
	/** @var FormFactory */
	private $factory;
	/** @var SupportOx */
	private $model;

	private $record;
		
	public function __construct(FormFactory $factory, \App\Model\SupportOx $supportOx) {
		$this->factory = $factory;
		$this->model = $supportOx;
	}

	public function create($record = null) {
		$form = $this->factory->create();
		$data = $form->addContainer('data');
		
		$data->addText('title', 'Jméno')
			 ->setRequired('Zadej jméno');

		$data->addInteger('priceReal', 'Vybráno')
			 ->setRequired('Zadej číslo');

		$data->addInteger('priceGoal', 'Cílová částka')
			 ->setRequired('Zadej číslo');

		$form->addHidden('record_id');
		$form->addSubmit('edit', 'Uložit');
	    $form->addSubmit('cancel', 'Zrušit')->setValidationScope([]);

	    if($record != null) {
	    	$form['data']->setDefaults($record);
	    	$form['record_id']->setValue($record->id);
	    }

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}

	public function formSucceeded(Form $form, $values) {
		if($form['cancel']->isSubmittedBy()) {
			return;
		}

		$this->model->update($values->record_id, $values->data);		
	}
}
