<?php

namespace App\AdminModule\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;
use Tracy\Debugger;

class ReferenceFormFactory {
	use Nette\SmartObject;
	
	/** @var FormFactory */
	private $factory;
	/** @var Reference */
	private $model;

	private $record;
		
	public function __construct(FormFactory $factory, \App\Model\Reference $reference) {
		$this->factory = $factory;
		$this->model = $reference;
	}

	public function create($record = null) {
		$this->record = $record;

		$form = $this->factory->create();
		$form->setHtmlAttribute('enctype', 'multipart/form-data');
		$data = $form->addContainer('data');
		
		$data->addText('author', 'Autor')
			 ->setRequired('Zadej autora');

		$data->addTextArea('text', 'Text');

	    $form->addSubmit('add', 'Přidat referenci');
	    $form->addSubmit('edit', 'Uložit referenci');
	    $form->addSubmit('cancel', 'Zrušit')->setValidationScope([]);

	    if($record != null) {
	    	$form['data']->setDefaults($record);
	    }

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}

	public function formSucceeded(Form $form, $values) {
		if($form['cancel']->isSubmittedBy()) {
			return;
		}

		if($this->record == null) {
			$values->data->position = $this->model->findAll()->max('position') + 1;
			$new_record = $this->model->insert($values->data);
		}
		else {
			$this->model->update($this->record->id, $values->data);
		}
	}
}
