<?php

namespace App\AdminModule\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;
use Tracy\Debugger;

class TourFormFactory {
	use Nette\SmartObject;
	
	/** @var FormFactory */
	private $factory;
	/** @var Tour */
	private $model;

	private $record;
		
	public function __construct(FormFactory $factory, \App\Model\Tour $tour) {
		$this->factory = $factory;
		$this->model = $tour;
	}

	public function create($record = null) {
		$this->record = $record;

		$form = $this->factory->create();
		$data = $form->addContainer('data');
		
		$data->addDatePicker('place_date', 'Datum')
			 ->setRequired('Zadej datum.');

		$data->addText('place', 'Místo')
			 ->setRequired('Zadej místo');

		$data->addText('place_genitive', 'Místo genitiv')
			 ->setRequired('Zadej místo v 2.pádu');			 

		$data->addText('place_locative', 'Místo lokál')
			 ->setRequired('Zadej místo v 6.pádu');

		$data->addText('accommodation', 'Ubytování');
		$data->addText('performance', 'Vystoupení');
		$data->addText('note', 'Poznámka');
		
		$data->addCheckbox('tent');
		$data->addCheckbox('shower');
		$data->addCheckbox('shower_bucket');
		$data->addCheckbox('accommodation_fee');
		$data->addCheckbox('free_day');
		$data->addCheckbox('move_flag');
		$data->addCheckbox('harinam_flag');

	    $form->addSubmit('add', 'Přidat místo');
	    $form->addSubmit('edit', 'Uložit místo');
	    $form->addSubmit('cancel', 'Zrušit')->setValidationScope([]);

	    if($record != null) {
	    	$form['data']->setDefaults($record);
	    }

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}

	public function formSucceeded(Form $form, $values) {
		if($form['cancel']->isSubmittedBy()) {
			return;
		}

		if($this->record == null) {
			$new_record = $this->model->insert($values->data);
		}
		else {
			$this->model->update($this->record->id, $values->data);
		}
	}
}
