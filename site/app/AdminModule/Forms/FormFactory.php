<?php

namespace App\AdminModule\Forms;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;


class FormFactory {
	use Nette\SmartObject;
	
	/**
	 * @return Form
	 */
	public function create() {
		$form = new Form;
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');

		$form->onError[] = array($this, 'formError');
		return $form;
	}

	public function formError(Form $form) {
		$errors = ["Form errors:" => $form->errors];
		//Debugger::fireLog($errors);
		//Debugger::dump($errors);exit;
	}
}
