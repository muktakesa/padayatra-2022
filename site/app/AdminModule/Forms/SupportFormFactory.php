<?php

namespace App\AdminModule\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Nette\Utils\Image;
use Nette\Utils\FileSystem;
use Tracy\Debugger;

class SupportFormFactory {
	use Nette\SmartObject;
	
	/** @var FormFactory */
	private $factory;
	/** @var Constants */
	private $model;

	private $record;
		
	public function __construct(FormFactory $factory, \App\Model\Constants $constants) {
		$this->factory = $factory;
		$this->model = $constants;
	}

	public function create() {
		$form = $this->factory->create();
		$data = $form->addContainer('data');
		$this->record = $this->model->findAll()->fetch();

		$data->addInteger('supportPriceReal', 'Vybráno')
			 ->setRequired('Zadej číslo');

		$data->addInteger('supportPriceGoal', 'Cílová částka')
			 ->setRequired('Zadej číslo');

		$form['data']->setDefaults($this->record);

		$form->addSubmit('save', 'Uložit');

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}

	public function formSucceeded(Form $form, $values) {
		$this->model->update($this->record->id, $values->data);		
	}
}
