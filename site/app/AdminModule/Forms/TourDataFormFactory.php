<?php

namespace App\AdminModule\Forms;

use Nette;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class TourDataFormFactory {
	use Nette\SmartObject;
	
	/** @var FormFactory */
	private $factory;
	/** @var Constants */
	private $model;

	private $record;
		
	public function __construct(FormFactory $factory, \App\Model\Constants $constants) {
		$this->factory = $factory;
		$this->model = $constants;
	}

	public function create($record = null) {
		$this->record = $record;

		$form = $this->factory->create();
		$data = $form->addContainer('data');
		
		$data->addDatePicker('tourDateStart', 'Datum');
		$data->addDatePicker('tourDateFinish', 'Datum');
		$data->addText('tourPlaceStart', 'Začátek trasy');
		$data->addText('tourPlaceFinish', 'Konec trasy');
		$data->addInteger('tourPlaceCount', 'Počet zastávek');
		$data->addInteger('tourKilometres', 'Počet kilometrů');
		
	    $form->addSubmit('save', 'Uložit');
	    $form->addSubmit('cancel', 'Zrušit')->setValidationScope([]);

	    if($record != null) {
	    	$form['data']->setDefaults($record);
	    }

		$form->onSuccess[] = array($this, 'formSucceeded');
		return $form;
	}

	public function formSucceeded(Form $form, $values) {
		$this->model->update($this->record->id, $values->data);
	}
}
