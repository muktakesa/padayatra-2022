<?php

namespace App\AdminModule\Presenters;
use Nette,
	App\Model;
use Tracy\Debugger;
use Nette\Application\UI\Form;
use App\AdminModule\Forms\TourFormFactory;
use App\AdminModule\Forms\TourDataFormFactory;

final class TourPresenter extends BasePresenter {        
    /** @var object */
    private $record;
    /** @var Tour */
    private $model;
    /** @var TourFormFactory @inject */
    public $factory;
    /** @var TourDataFormFactory @inject */
    public $tourDataFactory;

	protected function startup() {
		parent::startup();
		$this->model = $this->tour;
	}
    
	public function renderAdd() {
		$this->setView("form");
		$this->template->form_title = "Nové místo";
	}

    public function actionEdit($record_id) {
        $this->record = $this->model->get($record_id);
           
        if (!$this->record)
            throw new Nette\Application\BadRequestException("Místo nenalezeno.");
    }

	public function renderEdit($record_id) {
		$this->setView("form");
		$this->template->form_title = "Upravit místo";
        $this->template->record = $this->record;        
	}

    public function renderList() {
        $this->template->records = $this->model->findAll()
                                               ->order('place_date ASC');

        if($this->isAjax()) {
            $this->redrawControl('records');
        }
    }

    protected function createComponentForm() {
        $form = $this->factory->create($this->record);

        $form->onSuccess[] = function ($form, $values) {
            if($form['cancel']->isSubmittedBy()) {
                $form->getPresenter()->redirect('list');
                return $form;
            }
            
            if($this->record == null) { 
                $this->flashMessage("Místo bylo vytvořeno", 'success');
                $form->getPresenter()->redirect('list');
            }
            else {
                $this->flashMessage("Místo bylo upraveno", 'success');
                $form->getPresenter()->redirect('list');
            }
        };
        
        return $form;
    }

    protected function createComponentTourDataForm() {
        $constants = $this->constantsModel->findAll()->fetch();

        $form = $this->tourDataFactory->create($constants);

        $form->onSuccess[] = function ($form, $values) {
            $this->flashMessage("Uloženo.", 'success');
            $form->getPresenter()->redirect('list');
        };
        
        return $form;
    }

    public function actionDelete($id) {
        $this->payload->success = $this->model->delete($id);
        $this->sendPayload();
    }    

    public function actionSetActivity($record_id, $active) {
        $this->model->findBy(['id' => $record_id])
                    ->update(['active' => $active == "true" ? 1 : 0]);

        $this->sendPayload();
    }    
}
