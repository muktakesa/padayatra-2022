<?php

namespace App\AdminModule\Presenters;
use Nette,
	App\Model;
use Tracy\Debugger;
use App\AdminModule\Forms\SupportOxFormFactory;
use App\AdminModule\Forms\SupportFormFactory;

final class SupportPresenter extends BasePresenter {        
    /** @var object */
    private $record;
    /** @var SupportOx */
    private $model;
    /** @var SupportOxFormFactory @inject */
    public $formFactory;
    /** @var SupportFormFactory @inject */
    public $supportFormFactory;    

    protected function startup() {
        parent::startup();
        $this->model = $this->supportOxModel;
    }

    public function actionEdit($record_id) {
        $this->record = $this->model->get($record_id);
           
        if (!$this->record)
            throw new Nette\Application\BadRequestException("Místo nenalezeno.");
    }

    public function renderEdit($record_id) {
        $this->setView("form");
        $this->template->form_title = "Upravit místo";
        $this->template->record = $this->record;        
    }
    

    public function handleEdit($record_id) {
        $this->record = $this->model->get($record_id);
           
        if (!$this->record)
            throw new Nette\Application\BadRequestException("Záznam nenalezen.");

        $this['form']['data']->setDefaults($this->record);
        
        $this->redrawControl('recordForm');
    }

    public function renderList() {
        $this->template->records = $this->model->findAll()
                                               ->order('position ASC');
    }

    protected function createComponentForm() {
        $form = $this->formFactory->create($this->record);

        $form->onSuccess[] = function ($form, $values) {
            $this->flashMessage("Uloženo", 'success');
            $this->redrawControl('records');
        };
        
        return $form;
    }

    protected function createComponentSupportForm() {
        $form = $this->supportFormFactory->create();

        $form->onSuccess[] = function ($form, $values) {
            $this->flashMessage("Uloženo.", 'success');
            $this->sendPayload();
        };
        
        return $form;
    }
}
