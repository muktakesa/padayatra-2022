<?php

namespace App\AdminModule\Presenters;
ob_start(); // úprava kvůli tomu, že sign screen padá na PHP 7.0 >= s Cannot send session cache limiter - headers already sent
use Nette;
use App\Forms;
use App\AdminModule\Forms\SignInFormFactory;

class SignPresenter extends Nette\Application\UI\Presenter
{
	/** @var Forms\SignInFormFactory */
	private $signInFactory;

	public function __construct(SignInFormFactory $signInFactory)
	{
		$this->signInFactory = $signInFactory;
	}

	/**
	 * Sign-in form factory.
	 * @return Form
	 */
	protected function createComponentSignInForm()
	{
		return $this->signInFactory->create(function () {
			$this->redirect('Tour:list');
		});
	}	

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->redirect('in');
	}
}
