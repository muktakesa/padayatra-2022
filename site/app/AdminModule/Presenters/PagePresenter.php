<?php

namespace App\AdminModule\Presenters;
use Nette,
	App\Model;
use Tracy\Debugger;
use Nette\Application\UI\Form;
use Nette\Utils\Image;

final class PagePresenter extends BasePresenter {        
    
    protected $pageName;
    
    protected function startup()  {
        parent::startup();
    
        if (!$this->getUser()->isInRole('admin')) {
            $this->getUser()->logout();
            $this->redirect('Sign:in');
        }
    }
	
	public function actionForm($pageName) {
    	if($pageName != "") {
        	$this->pageName = $pageName;
    	}
	}
	
	public function renderForm($pageName) {
		$page = $this->page->findBy(['page' => $this->pageName])
		                   ->fetch();
        if (!$page)
            throw new Nette\Application\BadRequestException("Stránka '".$this->pageName."' nebyla nalezena.");                           

        $this->template->page = $page;
        
        $this["pageForm"]['data']->setDefaults($page);
        
        $_SESSION['KCFINDER'] = array(
            'disabled' => false,
            'uploadURL' => "../../../images/kcfinder",
		);
        
	}
					
    protected function createComponentPageForm(){
        $form = new Form;
        $data = $form->addContainer('data');

        $data->addText('title', 'Titulek')
             ->setRequired('Zadej titulek článku');

        $form->addUpload('image', 'Obrázek');
        $data->addRadioList('image_vertical_position', 'Zarovnání obrázku:', ['top' => 'nahoru', 'center' => 'na střed', 'bottom' => 'dolů']);

	    $data->addTextArea('text', 'Text:', 40);

	    $form->addSubmit('update', 'Uložit');
        $form->onSuccess[] = array($this, 'pageFormSucceeded');
        return $form;
    }
    
	public function pageFormSucceeded(Form $form, $values)	{
    	$page_record = $this->page->findBy(['page' => $this->pageName])
    	                   ->fetch();
        
        if($values->image->isOk() && $values->image->isImage()) {
            $image_filename = $page_record->id."_".$values->image->getSanitizedName();
            $image_filename = pathinfo($image_filename)['filename']."."."jpg";
            
            $image = $values->image->toImage();

            $this->page->saveImage($image, IMG_FOLDER."/".PAGE_FOLDER."/".$image_filename, null, null, null, 80, Image::JPEG);
            
            $page_record->update(['image' => $image_filename]);
        }

		$page_record->update($values['data']);

        $this->flashMessage('Text uložen.', 'success');
        $this->redirect('form', $this->pageName);
    }
}

