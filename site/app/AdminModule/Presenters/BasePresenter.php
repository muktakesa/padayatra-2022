<?php

namespace App\AdminModule\Presenters;

use Nette;
use App\Model;
use Tracy\Debugger;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {	
	/** @var User */
	protected $user;
	
	/** @var Tour */
	protected $tour;
	/** @var Constants */
	protected $constantsModel;
	/** @var Reference */
	protected $reference;
	/** @var SupportOx */
	protected $supportOxModel;
	/** @var Support */
	protected $supportModel;
	/** @var Page */
	protected $page;

	public function __construct(\App\Model\Tour $tour,
								\App\Model\Constants $constants,
								\App\Model\Reference $reference,
								\App\Model\SupportOx $supportOx,
								\App\Model\Support $support,
								\App\Model\Page $page) {

		parent::__construct();

		$this->tour = $tour;
		$this->constantsModel = $constants;
        $this->reference = $reference;
        $this->supportOxModel = $supportOx;
        $this->supportModel = $support;
        $this->page = $page;
	}

	protected function startup() {
		parent::startup();
		
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }		
		
        $this->user = $this->getUser()->getIdentity();

		\AdminModule\Forms\DatePicker\DatePicker::register();
	}

  	public function beforeRender() {
  		parent::beforeRender();
		$this->template->addFilter(null, 'Filters::initialize');
	}

    public function flashMessage($message, string $type = 'info'): \stdClass {
        if ($this->isAjax()) {
            $this->payload->messages[] = ['message' => $message,
                                          'type' => $type];
        }
        return parent::flashMessage($message, $type);
    }
}