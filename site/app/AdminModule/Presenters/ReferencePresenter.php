<?php

namespace App\AdminModule\Presenters;
use Nette,
	App\Model;
use Tracy\Debugger;
use Nette\Application\UI\Form;
use App\AdminModule\Forms\ReferenceFormFactory;

final class ReferencePresenter extends BasePresenter {        
    /** @var object */
    private $record;
    /** @var Reference */
    private $model;
    /** @var ReferenceFormFactory @inject */
    public $factory;

	protected function startup() {
		parent::startup();
		$this->model = $this->reference;
	}
    
    public function actionAdd() {
        $_SESSION['KCFINDER'] = array(
            'disabled' => false
        );
    }

	public function renderAdd() {
		$this->setView("form");
		$this->template->form_title = "Nová reference";
	}

    public function actionEdit($record_id) {
        $this->record = $this->model->get($record_id);
           
        if (!$this->record)
            throw new Nette\Application\BadRequestException("Reference nenalezeno.");
    }

	public function renderEdit($record_id) {
		$this->setView("form");
		$this->template->form_title = "Upravit referenci";
        $this->template->record = $this->record;        
	}

    public function renderList() {
        $this->template->records = $this->model->findAll()
                                               ->order('position ASC');

        if($this->isAjax()) {
            $this->redrawControl('records');
        }
    }

    protected function createComponentForm() {
        $form = $this->factory->create($this->record);

        $form->onSuccess[] = function ($form, $values) {
            if($form['cancel']->isSubmittedBy()) {
                $form->getPresenter()->redirect('list');
                return $form;
            }
            
            if($this->record == null) { 
                $this->flashMessage("Reference byla vytvořena", 'success');
                $form->getPresenter()->redirect('list');
            }
            else {
                $this->flashMessage("Reference byla upravena", 'success');
                $form->getPresenter()->redirect('list');
            }
        };
        
        return $form;
    }

    public function actionDelete($id) {
        $this->payload->success = $this->model->delete($id);
        $this->sendPayload();
    }    

    public function handleUpdatePosition($record_id, $new_position) {
        $this->payload->success = $this->model->updatePosition($record_id, $new_position);
    } 

    public function actionSetActivity($record_id, $active) {
        $this->model->findBy(['id' => $record_id])
                    ->update(['active' => $active == "true" ? 1 : 0]);

        $this->sendPayload();
    }    
}
