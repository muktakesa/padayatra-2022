document.addEventListener("DOMContentLoaded", function(event) { 
  	if(document.querySelector(".what-is-carousel") != null){
  		var whatIs = new Flickity( '.what-is-carousel', {
			wrapAround: true,
			autoPlay: 5000,
			prevNextButtons: false
		});
  	}

  	if(document.querySelector(".review-carousel") != null){
		var reviewCarousel = new Flickity( '.review-carousel', {
			wrapAround: true,
			autoPlay: 10000,
			pageDots: false,
			arrowShape: "M31.86,51c11.36,1.44,29.37,8.09,32.77,18.93L50.92,77.22c0-13-12.41-23.11-23.24-25.85V48.63c10.83-2.74,23.24-12.8,23.24-25.85l13.71,7.31C61.1,40.93,43.08,47.58,31.73,49"
		});
	}

	if(document.querySelector(".vystoupeni-carousel") != null){
  		var whatIs = new Flickity( '.vystoupeni-carousel', {
			wrapAround: true,
			autoPlay: 3000,
			prevNextButtons: false,
			fade: true
		});
  	}

  	if(document.querySelectorAll(".sliders progress") != null){
  		let bars = document.querySelectorAll(".sliders progress");
  		let values = document.querySelectorAll(".sliders .progressValue");
  		let maxes = document.querySelectorAll(".sliders .progressMax");
  		for (var i = bars.length - 1; i >= 0; i--) {
  			values[i].innerHTML = numberWithSpaces(bars[i].value);
  			maxes[i].innerHTML = numberWithSpaces(bars[i].max);
  		}
  	}


  	if(document.getElementById("map") != null){
  		let map = setupMapBox();

  		let legendButtons = document.querySelectorAll("#legend button");
		for (var i = legendButtons.length - 1; i >= 0; i--) {
			legendButtons[i].addEventListener("click", function(event) {
				highlightRoute(event.target.id, map);
			}); 
		}
  	}

  	let contactForm = document.getElementById("frm-contactForm");

  	if(contactForm) {
	  	contactForm.addEventListener("submit", function(event) {
	  		event.preventDefault();

		  	let data = new FormData(contactForm);
		  	let message = document.getElementById('contactFormMessage');
		  	data.set('workEmailConfirm', data.get('workPlace'));
			
			fetch(contactForm.action, {
			    method: 'post',
			    body: data
			})
			.then(response => {
				response.json().then(data => {
					if(data.success) {
				  		message.innerHTML = 'Zpráva byla odeslána, děkujeme.';
				  		contactForm.reset();
					}
				});
			})
			.catch(error => {
			  	message.innerHTML = 'Zprávu se nepodařilo odeslat. Zkontrolujte prosím všechna pole.';
			});
		});
  	}
});


function highlightRoute(route, map){
	let legendButtons = document.querySelectorAll("#legend button");
	for (var i = legendButtons.length - 1; i >= 0; i--) {
		map.setPaintProperty(legendButtons[i].id, 'line-color', 'rgb(111, 86, 123)');
		map.setPaintProperty(legendButtons[i].id+"label", 'text-opacity', .3);
		if (legendButtons[i].id == route) {
			legendButtons[i].classList.add("active");
		} else {
			legendButtons[i].classList.remove("active");
		}
	}
	map.setPaintProperty(route, 'line-color', 'rgb(255, 255, 255)');
	map.setPaintProperty(route+"label", 'text-opacity', 1);
}


function sendDonation(){
	let amount = parseInt(document.getElementById('donationAmount').value);
	if(amount == null || !Number.isInteger(amount)){
		alert("Zadaná hodnota není číslo.");
	} else if (amount < 500) {
		alert("Přes platební bránu není možné přispět méně než 500 Kč.");
	} else if (amount > 5000) {
		alert("Z technických důvodů je přes platební bránu možno přispět maximálně 5 000 Kč.");
	} else {
		window.open("https://www.pays.cz/paymentlink?prispevek-"+amount,'_blank');
	}
}


function toggleHelp(){
	let btn01 = document.getElementById("headingHelp01");
	let btn02 = document.getElementById("headingHelp02");
	let text01 = document.getElementById("help01");
	let text02 = document.getElementById("help02");
	if(btn01.classList.contains("active")){
		btn01.classList.remove("active");
		btn02.classList.add("active");
		text01.classList.add("hidden");
		text02.classList.remove("hidden");
	} else {
		btn02.classList.remove("active");
		btn01.classList.add("active");
		text02.classList.add("hidden");
		text01.classList.remove("hidden");
	}
}

function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

