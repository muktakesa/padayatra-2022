if($.fn.datepicker.dates !== undefined) {
    $.fn.datepicker.dates['cs'] = {
        days: ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"],
        daysShort: ["Ned", "Pon", "Úte", "Stř", "Čtv", "Pát", "Sob"],
        daysMin: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"],
        months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
        monthsShort: ["Led", "Úno", "Bře", "Dub", "Kvě", "Čer", "Čnc", "Srp", "Zář", "Říj", "Lis", "Pro"],
        today: "Dnes",
        clear: "Vymazat",
        monthsTitle: "Měsíc",
        weekStart: 1,
        format: "dd.mm.yyyy"
    };   
}

$(document).ready(function(){
    $('div.input-group.date-single').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language: 'cs',
        weekStart: 1,
        format: "dd.mm.yyyy",

        beforeShowDay: function(date) {
            var today = new Date().setHours(0,0,0,0);
            if(date.getTime() == today)
                return {classes: 'highlight'};
        }
    });


    $('.input-group.date input').keydown(function(event) {
        if(event.keyCode == 8 || event.keyCode == 46) {
            event.preventDefault();
            $(this).val("");
        }
    });

    if (typeof jQuery.validator !== "undefined") {
        jQuery.extend(jQuery.validator.messages, {
            required: "Toto pole je povinné.",
            number: "Prosím zadejte validní číslo.",
            min: jQuery.validator.format("Prosím zadejte čílo větší nebo rovno {0}."),
            max: jQuery.validator.format("Prosím zadejte čílo menší nebo rovno {0}."),
        });
    }

    $('form.validate').each(function() {
        $(this).validate();
    });
});


function initDateIntervalPicker(input_from, input_to) {
    var from = $(input_from);
    var to = $(input_to); 

    from.datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language: 'cs',
        format: "dd.mm.yyyy",

        beforeShowDay: function(date) {
            var today = new Date().setHours(0,0,0,0);
            if(date.getTime() == today)
                return { classes: 'highlight'};
        }
    })

    .on('changeDate',function(event){
        var date_from = from.data('datepicker').getDate();
        var date_to = to.data('datepicker').getDate();

        if(date_from > date_to) {
            to.datepicker('setDate', date_from);
        }               
    });

    to.datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        language: 'cs',
        format: "dd.mm.yyyy",        

        beforeShowDay: function(date) {
            var today = new Date().setHours(0,0,0,0);
            if(date.getTime() == today)
                return { classes: 'highlight'};
        }
    })

    .on('changeDate',function(event){
        var date_to = to.data('datepicker').getDate();
        var date_from = from.data('datepicker').getDate();

        if(date_to < date_from) {
            from.datepicker('setDate', date_to);
        }               
    });
}