var webalize = function (str) {
    var charlist;
    charlist = [
        ['Á','A'], ['Ä','A'], ['Č','C'], ['Ç','C'], ['Ď','D'], ['É','E'], ['Ě','E'],
        ['Ë','E'], ['Í','I'], ['Ň','N'], ['Ó','O'], ['Ö','O'], ['Ř','R'], ['Š','S'],
        ['Ť','T'], ['Ú','U'], ['Ů','U'], ['Ü','U'], ['Ý','Y'], ['Ž','Z'], ['á','a'],
        ['ä','a'], ['č','c'], ['ç','c'], ['ď','d'], ['é','e'], ['ě','e'], ['ë','e'],
        ['í','i'], ['ň','n'], ['ó','o'], ['ö','o'], ['ř','r'], ['š','s'], ['ť','t'],
        ['ú','u'], ['ů','u'], ['ü','u'], ['ý','y'], ['ž','z']
    ];
    for (var i in charlist) {
        var re = new RegExp(charlist[i][0],'g');
        str = str.replace(re, charlist[i][1]);
    }
    
    str = str.replace(/[^a-z0-9]/ig, '-');
    str = str.replace(/\-+/g, '-');
    if (str[0] == '-') {
        str = str.substring(1, str.length);
    }
    if (str[str.length - 1] == '-') {
        str = str.substring(0, str.length - 1);
    }
    
    return str.toLowerCase();
}

function initFooTable(table, success_message, error_message) {
    success_message = success_message || 'Záznam byl smazán.';
    error_message = error_message || 'Záznam se nepodařilo smazat.';

    var ft = FooTable.init(table, {
        'paging': {
            'enabled': true,
            'size': 20,
        },

        'sorting': {
            'enabled': true,
        },

        'filtering': {
            'enabled': true,
            'placeholder': "Vyhledat...",
            'dropdownTitle': "Hledat v:",
        },

        "state": {
            "enabled": true
        },
    });

    $(table).on('click', '.row-delete', function (event) {
        var invoker = this;
        var delete_name = $(invoker).data('delete_name');
        if(delete_name == undefined) {
            delete_name = " ";
        }
        else {
            delete_name = " \"" + delete_name + "\" ";
        }

        event.preventDefault();
        swal({
            title: "Opravdu si přejete záznam" + delete_name +"smazat?",
            text: "Tato operace je nevratná!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ano, smazat!",
            cancelButtonText: "Zrušit!",
            closeOnConfirm: false,
        }, function () {
            var row = $(invoker).parents('tr:first');
            row = FooTable.getRow(row);
            $.get(invoker.href, 
                function (payload) { 
                    $.nette.success(payload);
                    if(payload.success) {
                        row.delete();
                        swal("Smazáno!", success_message, "success");
                    }
                    else {
                        swal("Chyba!", error_message, "error");
                    }
                }
            );
        });
    });

    $(table).on("preinit.ft.state", function (e, ft, row) {
        $(table).css("visibility", "visible");
    });

    $(table).on("before.ft.filtering", function (e, ft, filters) {
        // odstranění diakritiky a ostatního z vyhledávání ve fulltextu
        filters.forEach(function(filter) {
            if(filter['name'] == "search") {
                filter['query']['parts'].forEach(function(part) {
                    part['query'] = webalize(part['query']);
                });
            }
        });
    });

    return ft;
}

function createFooTableFilter(column_name, prompt_value, options) {
    return FooTable.Filtering.extend({
        construct: function(instance){
            this._super(instance);
            this.statuses = options; // the options available in the dropdown
            this.def = prompt_value; // the default/unselected value for the dropdown (this would clear the filter when selected)
            this.$status = null; // a placeholder for our jQuery wrapper around the dropdown
        },

        $create: function(){
            this._super(); // call the base $create method, this populates the $form property
            var self = this, // hold a reference to my self for use later
                // create the bootstrap form group and append it to the form
                $form_grp = $('<div/>', {'class': 'form-group'})
                    .append($('<label/>', {'class': 'sr-only'}))
                    .prependTo(self.$form);

            // create the select element with the default value and append it to the form group
            self.$status = $('<select/>', { 'class': 'form-control' })
                .on('change', { self: self}, self._onStatusDropdownChanged)
                .append($('<option/>', { text: self.def}))
                .appendTo($form_grp);

            // add each of the statuses to the dropdown element
            $.each(self.statuses, function(i, status){
                self.$status.append($('<option value="' + i + '"/>').text(status));
            });
        },

        _onStatusDropdownChanged: function(e){
            var self = e.data.self, // get the MyFiltering object
                selected = $(this).val(); // get the current dropdown value

            if (selected !== self.def){ // if it's not the default value add a new filter
                self.addFilter(column_name, '"' + selected + '"', [column_name]);
            } else { // otherwise remove the filter
                self.removeFilter(column_name);
            }
            // initiate the actual filter operation
            self.filter();
        },

        draw: function(){
            this._super(); // call the base draw method, this will handle the default search input
            var status = this.find(column_name); // find the status filter
            if (status instanceof FooTable.Filter){ // if it exists update the dropdown to reflect the value
                this.$status.val(status.query.val().replace(/\"/g, ''));
            } else { // otherwise update the dropdown to the default value
                this.$status.val(this.def);
            }
        }
    });
}

var ID = function(elementId) {
    return document.getElementById(elementId);
};