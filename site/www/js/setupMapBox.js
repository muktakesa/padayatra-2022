function setupMapBox(map){
	mapboxgl.accessToken = 'pk.eyJ1IjoicGFwYXJlenppdCIsImEiOiJjazU2bXRwaDMwNHpoM2ZsamRuYjdqNHZvIn0.RdXdxVMIeAQRkoq1C7A5BQ';

  	map = new mapboxgl.Map({
	  container: 'map',
	  style: 'mapbox://styles/paparezzit/ckuzghlyo0f5s15npygt7a636',
	  center: [15.484123, 49.904546],
	  zoom: 7
	});

	let lineLayout = {
		'line-join': 'round',
		'line-cap': 'round'
	};

	let linePaint = {
		'line-color': 'rgb(111, 86, 123)',
		'line-width': 10
	};

	let labelLayout = {
		'text-field': ['get', 'year'],
		'text-font': ["Krishna Padayatra Regular","Arial Unicode MS Regular"],
		'text-size': 24
	};

	let labelPaint = {
		'text-color': 'rgb(255, 255, 255)',
		'text-opacity': .3,
	};





	// Zadávání souřadnic 

	map.on('load', () => {


		// 2015

		map.addSource('route2015', {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'geometry': {
					'type': 'LineString',
					"coordinates": [
				      [14.680248, 49.91193],
				      [14.806591, 49.915467],
				      [14.856029, 49.869462],
				      [14.932934, 49.865922],
				      [14.932934, 50.028523],
				      [15.268017, 49.996753],
				      [14.661022, 50.216926],
				      [15.262524, 50.127223],
				      [15.438305, 50.039108],
				      [15.438305, 50.201125],
				      [15.927196, 50.165951],
				      [16.015087, 50.032052],
				      [15.894237, 49.865922]
				    ],
				}
			}
		});

		map.addLayer({
			'id': 'route2015',
			'source': 'route2015',
			'type': 'line',
			'layout': lineLayout,
			'paint': linePaint
		});

		map.addSource('route2015label', {
			'type': 'geojson',
			'data': {
				"geometry": {
				    "coordinates": [ 15.4, 50.3],
				    "type": "Point"
				},
				"type": "Feature",
				"properties": {"year": 2015}
			}
		});

		map.addLayer({
			'id': 'route2015label',
			'type': 'symbol',
			'source': 'route2015label',
			'layout': labelLayout,
			'paint': labelPaint
		});


		// 2016

		map.addSource('route2016', {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'geometry': {
					'type': 'LineString',
					"coordinates": [
				      [14.7, 49.6],
				      [14.7, 49.5],
				      [14.8, 49.8],
				    ],
				}
			}
		});

		map.addLayer({
			'id': 'route2016',
			'source': 'route2016',
			'type': 'line',
			'layout': lineLayout,
			'paint': linePaint
		});

		map.addSource('route2016label', {
			'type': 'geojson',
			'data': {
				"geometry": {
				    "coordinates": [ 15.0, 49.6],
				    "type": "Point"
				},
				"type": "Feature",
				"properties": {"year": 2016}
			}
		});

		map.addLayer({
			'id': 'route2016label',
			'type': 'symbol',
			'source': 'route2016label',
			'layout': labelLayout,
			'paint': labelPaint
		});


		// 2017

		map.addSource('route2017', {
			'type': 'geojson',
			'data': {
				'type': 'Feature',
				'geometry': {
					'type': 'LineString',
					"coordinates": [
				      [14.3, 49.1],
				      [14.5, 49.2],
				      [14.1, 49.7],
				    ],
				}
			}
		});

		map.addLayer({
			'id': 'route2017',
			'source': 'route2017',
			'type': 'line',
			'layout': lineLayout,
			'paint': linePaint
		});

		map.addSource('route2017label', {
			'type': 'geojson',
			'data': {
				"geometry": {
				    "coordinates": [ 14.0, 49.4],
				    "type": "Point"
				},
				"type": "Feature",
				"properties": {"year": 2017}
			}
		});

		map.addLayer({
			'id': 'route2017label',
			'type': 'symbol',
			'source': 'route2017label',
			'layout': labelLayout,
			'paint': labelPaint
		});

	});


	// Aktivace kliknutí

	map.on('click', 'route2015', () => {
	  	highlightRoute("route2015", map);
	});

	map.on('click', 'route2015label', () => {
	  	highlightRoute("route2015", map);
	});

  	map.on('click', 'route2016', () => {
	  	highlightRoute("route2016", map);
	});

	map.on('click', 'route2016label', () => {
	  	highlightRoute("route2016", map);
	});

	map.on('click', 'route2017', () => {
	  	highlightRoute("route2017", map);
	});

	map.on('click', 'route2017label', () => {
	  	highlightRoute("route2017", map);
	});





	return(map);
}